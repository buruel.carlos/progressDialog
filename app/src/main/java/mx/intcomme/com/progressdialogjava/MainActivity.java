package mx.intcomme.com.progressdialogjava;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.zip.Inflater;

public class MainActivity
	extends Activity
{

	AlertDialog dialog;
	AlertDialog.Builder dialogBuilder;
	Button btnProgress;

	AlertDialog showProgressDialog(Context context, String message)
	{
		dialogBuilder = new AlertDialog.Builder(context);
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		if(inflater != null)
		{
			View dialogView = inflater.inflate(R.layout.progress_dialog, null);

			TextView textView = dialogView.findViewById(R.id.textView);
			textView.setText(message);

			dialogBuilder.setView(dialogView);
			dialogBuilder.setCancelable(true);

			dialogBuilder.setOnCancelListener(new DialogInterface.OnCancelListener() {
				@Override
				public void onCancel(DialogInterface d)
				{
					dialog = null;
				}
			});

			dialog = dialogBuilder.create();
			dialog.show();
		}
		return dialog;
	}

	void hideProgressDialog()
	{
		dialog.dismiss();
		dialog = null;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		btnProgress = findViewById(R.id.btnProgress);
		btnProgress.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(dialog == null)
				{
					String msg = "Mensaje";
					dialog = showProgressDialog(MainActivity.this, msg);
				}
				else
				{
					hideProgressDialog();
				}
			}
		});
	}
}
